/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   AbstractAlgorithm.h
 * Author: владислав
 *
 * Created on 14 сентября 2020 г., 17:37
 */

#ifndef ABSTRACTALGORITHM_H
#define ABSTRACTALGORITHM_H

class AbstractAlgorithm {
public:
    AbstractAlgorithm();
    AbstractAlgorithm(const AbstractAlgorithm& orig);
    virtual ~AbstractAlgorithm();
private:

};

#endif /* ABSTRACTALGORITHM_H */

